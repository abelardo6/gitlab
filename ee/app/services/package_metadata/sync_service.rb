# frozen_string_literal: true

module PackageMetadata
  class SyncService
    def self.execute
      # noop
      # 'service implemented as part of https://gitlab.com/gitlab-org/gitlab/-/merge_requests/109713'
    end
  end
end
